package firstPr.Sms;

import firstPr.HelloExm.Goodbye;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
    private static Logger logger = LogManager.getLogger(Main.class);
    public static void main(String[] args) {

        logger.trace(" trace message");
        logger.debug(" debug message");
        logger.info(" info message");
        logger.warn(" warn message");
        logger.error(" error message");
        logger.fatal(" fatal message");

    }
}
