package firstPr.FullNameExm;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FirstName {
    private static Logger log = LogManager.getLogger(FirstName.class);
    public static void main(String[] args) {

        log.trace("Trace");
        log.debug("Debug");
        log.info("Info");
        log.warn("Warn");
        log.error("Error");
        log.fatal("Fatal");
    }
}
