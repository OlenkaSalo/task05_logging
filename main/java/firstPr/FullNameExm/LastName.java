package firstPr.FullNameExm;
import java.lang.*;
public class LastName {
    public static void main(String[] args) {

        try {
            Class cls = Class.forName("LastName");

            ClassLoader cLoader = cls.getClassLoader();

            Class cls2 = Class.forName("java.lang.Thread", true, cLoader);

            // returns the name of the class
            System.out.println("Class = " + cls.getName());
            System.out.println("Class = " + cls2.getName());
        } catch(ClassNotFoundException ex) {
            System.out.println(ex.toString());
        }
    }

}

