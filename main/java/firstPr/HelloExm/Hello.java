package firstPr.HelloExm;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Hello {
    private static Logger LoggerH= LogManager.getLogger(Hello.class);
    public static void main(String[] args) {
        String message = "Hello there!";
        System.out.println(message);
        LoggerH.debug(message);
        LoggerH.info(message);
        LoggerH.error(message);
        LoggerChild.log();
    }

    private static class LoggerChild {
        private static final Logger childLogger = LogManager.getLogger(LoggerChild.class);

        static void log() {
            childLogger.debug("Hi Mom!");
        }
    }
}
